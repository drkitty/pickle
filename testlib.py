def test(test_list):
    def decorator(f):
        test_list.append(f)
    return decorator


class TestSession(object):
    def eq(self, left, right):
        if left != right:
            print("{} == {} failed".format(left, right))

    def ne(self, left, right):
        if left == right:
            print("{} != {} failed".format(left, right))

    def lt(self, left, right):
        if left >= right:
            print("{} < {} failed".format(left, right))
        pass

    def le(self, left, right):
        if left > right:
            print("{} <= {} failed".format(left, right))
        pass

    def gt(self, left, right):
        if left <= right:
            print("{} > {} failed".format(left, right))
        pass

    def ge(self, left, right):
        if left < right:
            print("{} >= {} failed".format(left, right))
        pass

    def in_(self, elem, obj):
        if elem not in obj:
            print("{} in {} failed".format(left, right))
        pass

    def inst(self, obj, cls):
        if not isinstance(obj, cls):
            print("{} isinstance {} failed".format(left, right))


def run_tests(test_list):
    t = TestSession()
    fail = []
    for test in test_list:
        print("==== {} ====".format(test.__name__))
        if not test(t):
            fail.append(test)
