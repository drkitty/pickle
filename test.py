#!/usr/bin/env python3


from testlib import run_tests, test


test_list = []


@test(test_list)
def bleh(t):
    t.eq(2 + 2, 4)
    t.ne(2 + 2, 5)


@test(test_list)
def blah(t):
    t.eq(6, 7)


if not run_tests(test_list):
    exit(1)
