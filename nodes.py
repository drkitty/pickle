from functools import wraps


FANCY_TUPLE_NONE = object()


def first(i):
    return next(iter(i))


class FancyTuple(object):
    def __init__(self, *args, **kwargs):
        i = 0
        while i < len(args):
            if i >= len(self._fields):
                raise Exception("Too many arguments (expected {})".format(
                    len(self._fields)
                ))
            name = self._fields[i]
            value = args[i]
            setattr(self, name, value)
            i += 1
        while i < len(self._fields):
            name = self._fields[i]
            value = kwargs.pop(name, FANCY_TUPLE_NONE)
            if value == FANCY_TUPLE_NONE:
                raise Exception("Field '{}' not initialized".format(name))
            else:
                if hasattr(self, name):
                    raise Exception("Field '{}' initialized twice".format(
                        name
                    ))
                setattr(self, name, value)
            i += 1
        if kwargs:
            raise Exception("Unexpected keyword argument '{}'".format(
                first(kwargs.keys())
            ))


def reverse_pairs(ps):
    return ((y,x) for (x,y) in ps)


class Node(FancyTuple):
    pass


class Literal(Node):
    _fields = ('value',)


class IntLiteral(Literal):
    pass


class StringLiteral(Literal):
    pass


class ListLiteral(Literal):
    pass


class Declaration(Node):
    _fields = ('name', 'value')


class Box(FancyTuple):
    _fields = ('constraint', 'value')

class TypeConstraint(FancyTuple):
    _fields = ('type',)


class Function(Node):
    _fields = ('name', 'body')
